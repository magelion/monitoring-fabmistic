#!/bin/bash

COMPONENT=wmbus_mqtt_forwarder
export SERIAL_PORT="/dev/ttyACM0"

PROCEXIST=$(ps aux | grep -v grep | grep $COMPONENT | wc -l)

if [ $PROCEXIST -eq 1 ]; then
    echo "$COMPONENT is already running";
    exit 1;
fi

TRANSCEIVER=$(lsusb | grep '1fc9:2047' | wc -l)
if [ $TRANSCEIVER -eq 0 ]; then
    echo "WMBus Transceiver not found on host";
    exit 1;
fi

export STATIONID="0000"$(cat /sys/class/net/eth0/address | sed "s/://g")

# 149.202.167.223 prod-mqtt-1
# 149.202.178.243 prod-mqtt-2

export MQTT_BROKER_1="130.190.30.227"
export MQTT_BROKER_2="149.100.100.243"

export MQTT_URL="mqtt://$MQTT_BROKER_1"
export MQTT_TOPIC="xnet/wmbus"

export PARAMS="$STATIONID $SERIAL_PORT $MQTT_URL $MQTT_TOPIC"

export PATH=$PATH:/usr/local/bin
export NODE_PATH=$NODE_PATH:/usr/local/lib/node_modules
export WORKDIR=/home/pi/$COMPONENT 
export APP=$WORKDIR/${COMPONENT}.js

echo "export passed"

# OUTPUT=/dev/null
OUTPUT=nohup.out
node $APP $PARAMS &>$OUTPUT
