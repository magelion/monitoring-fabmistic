/*
WMBus Forwarder
Should installed on the host equipped of a Adeunis WMBus transceiver
(c) Didier Donsez and Vivien Quéma 2016
*/


// TODO test process.argv.length

var STATION_UID = process.argv[2];
var WMBUS_PORT = process.argv[3];
var MQTT_URL = process.argv[4];
var MQTT_TOPIC = process.argv[5];

// TODO Modes T1, T2, R1, R2, S1, S1-m, S2
// TODO add transceiver model ARF8020
// TODO add freq

var buf = undefined;
var bufIndex = 0;
var frameLen = -1;
var state = false;

function processData(data, callback) {
   console.log('start processe data');
    if(data.length === 0) {
       console.log('no data');
       return;
      }
   var s = 0;

    while(s < data.length) {
      while(state===false && s < data.length) { // start the parsing of a frame
         if(data[s] === 0xFF) {
            state = true;
         }
         s++;
      }

      if(state===true && s < data.length) {
         if(frameLen < 0) {
            frameLen = 1+1+1+2+6+1+data[s]+1;
            if(frameLen > 138) {
               state = false;
               frameLen = -1;
               s++;
            } else {
               // Trouver la temperature
               
               buf = new Buffer(frameLen);
               buf.fill(0xFF);
               bufIndex = 0;
               buf[bufIndex] = 0xFF;
               bufIndex++;
            }
         } else {
            if(frameLen - bufIndex > data.length - s ) {
               data.copy(buf, bufIndex, s, data.length);
               bufIndex += (data.length - s);
               s = data.length;
            } else {
               data.copy(buf, bufIndex, s, frameLen+s); 
               // bufIndex += frameLen - 1;
               s += frameLen - 1;
               callback(buf);

               state = false;
               buf = undefined;
               bufIndex = 0;
               frameLen = -1;
            }
         }
      }
   }
}



var mqtt = require('mqtt');
var client = mqtt.connect(MQTT_URL, { keepalive: 0 });
var mqttConnected = false;

client.on('connect', function () {
 mqttConnected = true;
 console.log('[WMBUS] MQTT connected to ',MQTT_URL);
});

client.on('reconnect', function () {
 mqttConnected = true;
 console.log('[WMBUS] MQTT reconnected to ',MQTT_URL);
});

client.on('offline', function () {
 mqttConnected = false;
 console.log('[WMBUS] MQTT offline ');
});

client.on('close', function () {
 mqttConnected = false;
 console.log('[WMBUS] MQTT closed');
}); 

client.on('error', function (error) {
 mqttConnected = false;
 console.log('[WMBUS] MQTT Error ',error);
});

var SerialPort = require("serialport");

var port = new SerialPort(WMBUS_PORT, {
 baudRate: 115200,
 parser: SerialPort.parsers.raw
});

port.on('data', function (data) {
  console.log('[WMBUS] Data : ' + data.toString('hex'));
  processData(data, function(frame){
    if(mqttConnected) {
   var msg = {};
      msg.station = STATION_UID;
 msg.protocol = "wmbus";
 msg.freq = 868;
      msg.frame = frame.toString('hex');
 console.log('[WMBUS] Emit : ' + JSON.stringify(msg));
      client.publish(MQTT_TOPIC, JSON.stringify(msg));
    }
  });
});

port.on("open", function () {
   console.log('[WMBUS] Port open');
});
port.on("close", function (e) {
   console.log('[WMBUS] Port close: '+ e);
}); 
port.on("error", function (e) {
   console.log('[WMBUS] Port error : '+ e);
});
port.on("disconnect", function (e) {
   console.log('[WMBUS] Port isconnect : '+ e);
});

