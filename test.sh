#!/bin/bash

COMPONENT=wmbus_mqtt_forwarder
export SERIAL_PORT="/dev/ttyACM0"

PROCEXIST=$(ps aux | grep -v grep | grep $COMPONENT | wc -l)

if [ $PROCEXIST -eq 1 ]; then
    echo "$COMPONENT is already running";
    exit 1;
fi

TRANSCEIVER=$(lsusb | grep '1fc9:2047' | wc -l)
if [ $TRANSCEIVER -eq 0 ]; then
    echo "WMBus Transceiver not found on host";
    exit 1;
fi

echo "test passed"

export STATIONID="0000"$(cat /sys/class/net/eth0/address | sed "s/://g")
